<?php session_start();?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="styles/index.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
  <title>Immersailles</title>
  <?php
  require_once("class/Marker.php");
  require_once("class/Objects.php");
  $db = new Database();
  $dbh = $db->connect();
  ?>
</head>

<body>
  <header class="header">
    <div class="logo">
      <img src="images/logo.png" alt="Logo">
      <h1>Immersailles</h1>
    </div>
  </header>
  <div class="corps">
    <?php
        if (isset($_GET['level'])) {
          $y = $_GET['year'];
          $requete = "SELECT `id_year` FROM `years` WHERE `year` = ' $y '";
          $resultat = $dbh->query($requete);
          $r = $resultat->fetch();
          $level = $_GET['level'];
          $requete2 = "SELECT `level` FROM `maps` WHERE `level` = ' $level '";
          $resultat2 = $dbh->query($requete2);
          $r2 = $resultat2->fetch();
          $requete3 = "SELECT `file_name` FROM `maps` WHERE `year_id` = " . $r['id_year'] . " AND `level` = " . $r2['level'] . "";
          $resultat3 = $dbh->query($requete3);
          $r3 =  $resultat3->fetch();
          if (isset($r3["file_name"])) $dim = getimagesize('./images/plans/' . $r3["file_name"]);
          echo '
                  <span class="titre"> Etage : '.$r2['level'].'</span>
                ';
        }
    ?>
  </div>
  

  <div class="page">
    <div class="map">
      <div id="leaflet">
        <!-- MAP LEAFLET -->
      </div>
      
      <script>
        <?php echo "var x = $dim[0];" ?>
        <?php echo "var y = $dim[1];" ?>
        var bounds = [
          [0, 0],
          [y, x]
        ];
        //Doit s'appeler map pour la fonction printMarkers
        var map = L.map('leaflet', {
          crs: L.CRS.Simple,
          minZoom: -5,
        });

        map.on('click', function(e){
          var e = document.getElementById('description');
          if(e.style.display == 'block')
            e.style.display = 'none';
        })

        //Doit s'appeler icone pour la fonction printMarkers
        var icone = L.icon({
          iconUrl: './images/marker.png',
          iconAnchor: [19, 50], // point of the icon which will correspond to marker's location
          popupAnchor: [0, -45] // point from which the popup should open relative to the iconAnchor
        });

        <?php
        if (isset($r3["file_name"])) echo 'var image = L.imageOverlay("./images/plans/' . $r3["file_name"] . '", bounds).addTo(map);';
        $marker = new Marker;
        $marker->printMarkersIndex($dbh, $r3["file_name"]);
        ?>
        //Affiche le centre de la map 
        map.setView([y / 2, x / 2], 1);
      </script>
      <?php
      if (!isset($_GET['year']) && !isset($_GET['level'])) {
        $requete = "SELECT `year`, `level` FROM `maps`, `years` WHERE `year_id`=`id_year` ORDER BY `year`";
        $resultat = $dbh->query($requete);
        $r = $resultat->fetch();

        echo '<script language="Javascript">
                    document.location.replace("?year=' . $r['year'] . '&level=' . $r['level'] . '");
                </script>';
      } else if (isset($_GET['year']) && !isset($_GET['level'])) {
        $requete = "SELECT `id_year` FROM `years` WHERE `year` = " . $_GET['year'] . "";
        $resultat = $dbh->query($requete);
        $r = $resultat->fetch();

        $requete2 = "SELECT `level` FROM `maps` WHERE `year_id` = " . $r['id_year'] . "";
        $resultat2 = $dbh->query($requete2);
        $r2 = $resultat2->fetch();
        echo '<script language="Javascript">
                    document.location.replace("?year=' . $_GET['year'] . '&level=' . $r2['level'] . '");
                </script>';
      }
      ?>
      <div style="position:absolute;z-index:1;margin-top: 5%;" class="card" style="width:6rem;">
        <div class="card-body">
          <b>
            <u>
              <p class="card-title" style="font-size: 0.85em;">Niveaux</p>
            </u>
          </b>
          <?php

          if (isset($_GET['year'])) {
            $y = $_GET['year'];
            $requete = "SELECT `id_year` FROM `years` WHERE `year` = ' $y '";
            $resultat = $dbh->query($requete);
            $r = $resultat->fetch();
            $requete2 = "SELECT `level` FROM `maps` WHERE `year_id` = " . $r['id_year'] . " ORDER BY `level`";
            $resultat2 = $dbh->query($requete2);
            while ($r2 =  $resultat2->fetch()) {
              echo '<p style="margin-bottom: 0px;">
                            <a style="color:black; font-size: 0.80em;" id="' . $r2['level'] . '" href="/~bgueffie/opendata/?year=' . $y . '&level=' . $r2['level'] . '">Niveau ' . $r2['level'] . '</a>
                        </p>';
            }
          }
          ?>
          <script>
            var niveau = <?php $lev = $_GET['level'];
                          echo $lev; ?>;
            if (!document.getElementById(niveau.toString()).classList.contains('bold')) {
              document.getElementById(niveau.toString()).classList.add('bold');
            }
          </script>
        </div>
      </div>

    </div>
    <div class="description" id="description" style="display: none;">
    </div>
  </div>
  <ul class="container">
    <div class="timeline">
      <?php
      $requete = "SELECT COUNT(DISTINCT(`id_year`)) c FROM `years`,`maps` WHERE `id_year` =`year_id`";
      $count = $dbh->query($requete);
      $count = $count->fetch();
      $length = 100 / $count['c'];
      $requete = "SELECT DISTINCT(`year`) FROM `years`,`maps` WHERE `id_year` =`year_id` ORDER BY `year`";
      $resultat = $dbh->query($requete);
      while ($r = $resultat->fetch()) {
        echo '<li style="width:' . $length . '%;" id="' . $r['year'] . '"><a href="/~bgueffie/opendata/?year=' . $r['year'] . '" style="color:black;">' . $r['year'] . '</a></li>';
      }
      ?>
      <script>
        var date = <?php $y = $_GET['year'];
                    echo $y; ?>;
        if (!document.getElementById(date.toString()).classList.contains('active')) {
          document.getElementById(date.toString()).classList.add('active');
        }
      </script>
    </div>
  </ul>



  <div class="footer">
    <div class="text">
      <div>
        <span class="dore">Crédits</span>
        <span>Gandega Issa</span>
        <span>Francisco Michel</span>
        <span>Gueffier Benjamin</span>
      </div>
      <div>
        <span class="dore">Connexion</span>
        <span><a href="auth/login.php" style="color:white;">Connectez-vous</a></span>
      </div>
      <div>
        <span class="dore">En savoir plus</span>
        <span><a href="https://bitbucket.org/Michel_Francisco/opendata/src/master/" target="_blank" style="color:white;">Lien GIT</a></span>
      </div>
    </div>
    <span class="copyright">Copyright © 2020 x Inc. Tous droits réservés</span>
  </div>
</body>

</html>