<!DOCTYPE html>
<html>
<?php
require_once("../class/User.php");
?>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../styles/login.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  <title>Mot de passe oublié</title>
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Mot de passe oublié</h5>
            <form class="form-signin" method="post">
              <div class="form-group">
                <label for="inputEmail1">Adresse e-mail</label>
                <input type="email" class="form-control" id="inputEmail1" name="email" placeholder="Entrez votre adresse e-mail">
              </div>
              <button class="btn btn-lg btn-block text-uppercase" id="button-login" type="submit">Envoyer un mail</button>
              <br>
              <?php
              if (isset($_POST["email"])) {
                $user = new User;
                $msg = $user->mail($_POST["email"]);
                echo $msg;
              }
              ?>
              <a href="login.php" style="color:#c9ae81;">Retour</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>