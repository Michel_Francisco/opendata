<?php
include("Database.php");
include("Objects.php");
class Marker
{
    private $id_marker = NULL;
    private $latX = NULL;
    private $longY = NULL;
    private $pic_marker = NULL;
    private $id_creator = NULL;
    private $session_creator = NULL;
    private $obj_id = NULL;
    private $year_id = NULL;
    private $level = NULL;

    function addMarker($longi, $latit, $name_obj, $idmap)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "SELECT `id_obj` FROM `objects` WHERE `name_obj` = '$name_obj'";
        $res = $dbh->query($req);
        $r = $res->fetch();
        $obj_id = $r['id_obj'];
        $user = $_SESSION['user'];
        $session = $_SESSION['session_id'];
        $req = "INSERT INTO `markers` (`id_marker`, `lat/x`, `long/y`, `id_creator`, `session_creation`, `obj_id`, `map_id`) VALUES (NULL, '$latit', '$longi', '$user', '$session','$obj_id', '$idmap')";
        $dbh->query($req);
        return '<div class="alert alert-success" role="alert">Marker ajouté avec succès !</div>';
    }

    function printMarkersIndex($dbh,$map){ //Affichage des marqueurs dans l'index
        $req = $dbh->query("SELECT `id_map` FROM `maps` WHERE `file_name` = '$map'");
        $r = $req->fetch();
        $_SESSION['map'] = $map;
        $idmap = $r['id_map'];
        $req2 = $dbh->query("SELECT * FROM `markers`,`objects` WHERE `obj_id` = `id_obj` AND `map_id` = '$idmap'");
        while ($r2 = $req2->fetch()) {
            $lat = $r2['lat/x'];
            $long = $r2['long/y'];
            $name = $r2['name_obj'];
            $id_marker = $r2['id_marker'];
            $obj = new Objects;
            $data = $obj->getObjectData($id_marker);
            $id_obj = $obj->getObjectId($id_marker);
            $type = $obj->getObjectType($data, $id_obj);
            //Affichage different en fonction du type de l'objet recupéré car les informations disponibles sur wikidata varient en fonction du type
            if($type=='Personne') $text = $obj->displayPeople($data, $id_obj); 
            else if($type=='Piece')  $text = $obj->displayRoom($data, $id_obj); 
            else if($type=='Objet') $text = $obj->displayObject($data, $id_obj);
            echo "L.marker([$lat,$long], {icon: icone}).addTo(map).on('click', function(e) {
                var e = document.getElementById('description');
                       if(e.style.display == 'none')
                          e.style.display = 'block';
                $('.description').html(`".$text."`);
                });";
        }
    }

    function printMarkersAdmin($dbh,$map){//Affichage des marqueurs dans la page admin
        $req = $dbh->query("SELECT `id_map` FROM `maps` WHERE `file_name` = '$map'");
        $r = $req->fetch();
        $_SESSION['map'] = $map;
        $idmap = $r['id_map'];
        $req2 = $dbh->query("SELECT * FROM `markers`,`objects` WHERE `obj_id` = `id_obj` AND `map_id` = '$idmap'");
        while ($r2 = $req2->fetch()) {
            $lat = $r2['lat/x'];
            $long = $r2['long/y'];
            $name = $r2['name_obj'];
            $id = $r2['id_marker'];
            $id_creator = $r2['id_creator'];
            $requete = $dbh->query("SELECT `email` FROM `users` WHERE ".$id_creator." = `id_user`");
            $res = $requete->fetch();

            echo "L.marker([$lat,$long], {icon: icone}).addTo(map).on('click', function(e) {
                var id = this.options.id;
                $('#popupDelete').html(`
                    <div aria-labelledby=\"markerLabel" . $id . "\" class=\"modal left fade\" id=\"modalMarker". $id . "\" role=\"dialog\" tabindex=\"-1\">
                        <form id=\"formDelete\" method=\"POST\" action=\"\">
                            <input type=\"hidden\" name=\"id_delete_marker\" value=\"". $id ."\" >
                            <div class=\"modal-dialog\" role=\"document\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\">
                                        <h5 class=\"modal-title\" id=\"modalMarkerTitle\">Suppression du marqueur : " . $name . "</h5>
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                                        <span aria-hidden=\"true\">&times;</span>
                                        </button>
                                    </div>
                                    <div class=\"modal-body\">
                                        <p>
                                          <b>Info du marqueur :</b><br>
                                          
                                          Id du marqueur : ".$id."<br>
                                          Email du créateur : ".$res["email"]."<br>
                                          Id du créateur : ".$id_creator."
                                        </p>
                                    </div>
                                    <div class=\"modal-footer\">
                                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Annuler</button>
                                        <button type=\"submit\" class=\"btn btn-danger\">Suppression</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>`);
                $('#modalMarker". $id . "').modal('show');
                map.setView(e.target.getLatLng());
            });";
        }
    }
}
