<?php
include("../class/Map.php");
class Year
{
    private $id_year = NULL;
    private $year = NULL;

    function addYear($year, $dbh)
    {
        $req = "INSERT INTO `years` (`id_year`, `year`) VALUES (NULL, '$year')";
        $dbh->query($req);
        return '<div class="alert alert-success" role="alert">Année ajoutée avec succès !</div>';
    }

    function verifYear($year, $dbh)
    {
        $verif = "SELECT `year` FROM `years` WHERE `year` = '$year'";
        $res = $dbh->query($verif);
        $res = $res->fetch();
        if ($res['year'] == 0) return 1; //1 => n'existe pas dans la bdd
        return 0; //0 => existe dans la bdd
    }

    function updateYear($dbh,$yearEdit,$newYear){
        $result = $dbh->query("UPDATE `years` SET `year` = '$newYear' WHERE `years`.`id_year` = '$yearEdit'");
        return '<div class="alert alert-success" role="alert">Année modifiée avec succès !</div>';
    }

    function deleteYear($year_id)
    {
        $db = new Database();
        $dbh = $db->connect();
        $requete = "SELECT `file_name` FROM `maps` WHERE year_id='$year_id'";
        $res = $dbh->query($requete);
        $r = $res->fetch();
        if ($r != '') {
            unlink('../images/plans/' . $r['file_name'] . '');
        }
        $requete = "DELETE FROM `years` WHERE id_year='$year_id'";
        $dbh->query($requete);
        
        $requete = "SELECT `id_map` FROM `maps` WHERE year_id='$year_id'";
        $res = $dbh->query($requete);
        while($r = $res->fetch()){
            deleteMap($r['id_map']);
        }
        return '<div class="alert alert-success" role="alert">Année supprimée !</div>';
    }
}
