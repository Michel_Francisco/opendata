<?php
class Objects
{
    private $id_obj = NULL;
    private $name_obj = NULL;
    private $pic_obj = NULL;
    private $type_obj = NULL;
    private $date_arriv = NULL;
    private $date_leav = NULL;
    private $localisation = NULL;
    private $desc_obj = NULL;

    function addObj($type, $label, $wikidata)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "SELECT COUNT(`id_obj`) FROM `objects` WHERE `type_obj` = '$type' AND `name_obj` = '$label' OR `source` = '$wikidata'";
        $res = $dbh->query($req);
        $r = $res->fetch();
        if ($r[0] != '0') {
            return '<div class="alert alert-danger" role="alert">Cet Objet a déjà été référencé !</div>';
        }
        $req = "INSERT INTO `objects` (`id_obj`, `name_obj`, `type_obj`, `source`) VALUES (NULL, '$label', '$type','$wikidata')";
        $dbh->query($req);
        return '<div class="alert alert-success" role="alert">Objet historique ajouté avec succès !</div>';
    }

    function addLink($label, $link)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "SELECT `id_obj` FROM `objects` WHERE `name_obj` = '$label'";
        $res = $dbh->query($req);
        $r = $res->fetch();
        $obj_id = $r['id_obj'];
        $req = "INSERT INTO `links` (`id_link`, `link`, `obj_id`) VALUES (NULL, '$link', '$obj_id')";
        $res = $dbh->query($req);
    }

    function getObjectData($id_marker)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = $dbh->query("SELECT `source` FROM `objects`, `markers` WHERE `id_obj` = `obj_id` AND `id_marker` = '$id_marker'");
        $r = $req->fetch();
        $source = $r['source'];
        $num = substr($source, 30);
        $link = "https://www.wikidata.org/wiki/Special:EntityData/" . $num . ".json";
        $object = file_get_contents($link);
        $data = json_decode($object);
        return $data;
    }

    function getObjectLinks($id_obj)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "SELECT `id_obj` FROM `objects` WHERE `source` LIKE '%$id_obj'";
        $res = $dbh->query($req);
        $r = $res->fetch();
        $id = $r['id_obj'];
        $req = $dbh->query("SELECT `link` FROM `links` WHERE `obj_id` = $id");
        $links_array = array();
        while ($r = $req->fetch()) {
            $links_array[] = $r['link'];
        }
        $links = "";
        for ($i = 0; $i < count($links_array); $i++) {
            $links .= '<a href=' . $links_array[$i] . ' target="_blank" style="color:white;">' . $links_array[$i] . '</a><br>';
        }
        return $links;
    }

    function getIdData($id_obj)
    {
        $link = "https://www.wikidata.org/wiki/Special:EntityData/" . $id_obj . ".json";
        $object = file_get_contents($link);
        $data = json_decode($object);
        return $data;
    }

    function getObjectId($id_marker)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = $dbh->query("SELECT `source` FROM `objects`, `markers` WHERE `id_obj` = `obj_id` AND `id_marker` = '$id_marker'");
        $r = $req->fetch();
        $source = $r['source'];
        $num = substr($source, 30);
        return $num;
    }

    function getObjectPicture($data, $id_obj)
    {
        $img_str =  $data->entities->$id_obj->claims->P18[0]->mainsnak->datavalue->value;
        $img_mod = str_replace(" ", "_", $img_str);
        $img_md5 = md5($img_mod);
        $img = "https://upload.wikimedia.org/wikipedia/commons/" . substr($img_md5, 0, 1) . "/" . substr($img_md5, 0, 2) . "/" . $img_mod;
        return $img;
    }

    function getObjectImage($data, $id_obj)
    {
        $img_str =  $data->entities->$id_obj->claims->P18[0]->mainsnak->datavalue->value;
        $img_mod = str_replace(" ", "_", $img_str);
        $img_md5 = md5($img_mod);
        $img = "//upload.wikimedia.org/wikipedia/commons/" . substr($img_md5, 0, 1) . "/" . substr($img_md5, 0, 2) . "/" . $img_mod;
        return $img;
    }

    function getObjectName($data, $id_obj)
    {
        $obj_name = $data->entities->$id_obj->claims->P373[0]->mainsnak->datavalue->value;
        return $obj_name;
    }

    function getObjectClass($data, $id_obj)
    {
        $obj = new Objects;
        $obj_id = $data->entities->$id_obj->claims->P31[0]->mainsnak->datavalue->value->id;
        $obj_data = $obj->getIdData($obj_id);
        $obj_type = $obj->getObjectName($obj_data, $obj_id);
        return $obj_type;
    }

    function getObjectType($data, $id_obj)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = $dbh->query("SELECT `type_obj` FROM `objects` WHERE `source` LIKE '%$id_obj'");
        $r = $req->fetch();
        $type = $r['type_obj'];
        return $type;
    }

    function getObjectCreator($data, $id_obj)
    {
        $obj = new Objects;
        $obj_id = $data->entities->$id_obj->claims->P170[0]->mainsnak->datavalue->value->id;
        $obj_data = $obj->getIdData($obj_id);
        $obj_crea = $obj->getObjectName($obj_data, $obj_id);
        return $obj_crea;
    }

    function getObjectDesc($data, $id_obj)
    {
        $obj_desc = $data->entities->$id_obj->descriptions->fr->value;
        return $obj_desc;
    }

    function getObjectBirthDate($data, $id_obj)
    {
        $date_nai = $data->entities->$id_obj->claims->P569[0]->mainsnak->datavalue->value->time;
        $year = substr($date_nai, 1, 4);
        $month = substr($date_nai, 6, 2);
        $day = substr($date_nai, 9, 2);
        $text = $day . '/' . $month . '/' . $year;
        return $text;
    }

    function getObjectDeathDate($data, $id_obj)
    {
        $date_mor = $data->entities->$id_obj->claims->P570[0]->mainsnak->datavalue->value->time;
        $year = substr($date_mor, 1, 4);
        $month = substr($date_mor, 6, 2);
        $day = substr($date_mor, 9, 2);
        $text = $day . '/' . $month . '/' . $year;
        return $text;
    }

    function getObjectLocation($data, $id_obj)
    {
        $obj = new Objects;
        $obj_id = $data->entities->$id_obj->claims->P276[0]->mainsnak->datavalue->value->id;
        $obj_data = $obj->getIdData($obj_id);

        $obj_loc = $obj->getObjectName($obj_data, $obj_id);
        return $obj_loc;
    }

    function displayRoom($data, $id_obj)
    {
        $obj = new Objects;
        $obj_name = $obj->getObjectName($data, $id_obj);
        $obj_type = $obj->getObjectClass($data, $id_obj);
        $obj_desc = $obj->getObjectDesc($data, $id_obj);
        $img_url = $obj->getObjectPicture($data, $id_obj);
        $links = $obj->getObjectLinks($id_obj);
        $text =  "
    <div class='photo_objet' style=\"width: 20vw;
      height: 25vh;
      display: inline-flex;
      float:right;
      align-items: flex-end;
      justify-content: flex-end;
      color: white;
      background: url(" . $img_url . "); background-size: cover;\">
        <span>" . $obj_name . "</span>
      </div>
      <div class=\"info\">
        <span class=\"normal\"><span class=\"dore\">Type d'objet: </span><span class=\"ital\">" . $obj_type . "</span></span>
        <span class=\"normal\"><span class=\"dore\">Description:</span></span>
        <span class=\"normal\">" . $obj_desc . "</span>
        <span class=\"normal\"><span class=\"dore\">Liens utiles:</span></span>
        <span class=\"normal\">" . $links . "</span>
      </div>
      ";
        return $text;
    }

    function displayPeople($data, $id_obj)
    {
        $obj = new Objects;
        $obj_name = $obj->getObjectName($data, $id_obj);
        $obj_desc = $obj->getObjectDesc($data, $id_obj);
        $obj_type = $obj->getObjectClass($data, $id_obj);
        $links = $obj->getObjectLinks($id_obj);
        $img_url = $obj->getObjectImage($data, $id_obj);
        $date_nai = $obj->getObjectBirthDate($data, $id_obj);
        $date_mor = $obj->getObjectDeathDate($data, $id_obj);
        $text = "
        <div class='photo_objet' style=\"width: 20vw;
        height: 25vh;
        display: inline-flex;
        align-items: flex-end;
        justify-content: flex-end;
        float:right;
        color: white;
        background: url(" . $img_url . "); background-size: cover;\">
            <span>" . $obj_name . "</span>
        </div>
        <div class=\"info\">
            <span class=\"normal\"><span class=\"dore\">Type d'objet: </span><span class=\"ital\">" . $obj_type . "</span></span>
            <span class=\"normal\"><span class=\"dore\">Date de naissance: </span><span class=\"ital\">" . $date_nai . "</span></span>
            <span class=\"normal\"><span class=\"dore\">Date de décès: </span><span class=\"ital\">" . $date_mor . "</span></span>
            <span class=\"normal\"><span class=\"dore\">Description:</span></span>
            <span class=\"normal\">" . $obj_desc . "</span>
            <span class=\"normal\"><span class=\"dore\">Liens utiles:</span></span>
            <span class=\"normal\">" . $links . "</span>
        </div>
        ";
        return $text;
    }

    function displayObject($data, $id_obj)
    {
        $obj = new Objects;
        $links = $obj->getObjectLinks($id_obj);
        $obj_name = $obj->getObjectName($data, $id_obj);
        $img_url = $obj->getObjectImage($data, $id_obj);
        $obj_type = $obj->getObjectClass($data, $id_obj);
        $obj_loc = $obj->getObjectLocation($data, $id_obj);
        $obj_crea = $obj->getObjectCreator($data, $id_obj);
        $text =  "
    <div class='photo_objet' style=\"width: 20vw;
      height: 25vh;
      display: inline-flex;
      align-items: flex-end;
      float:right;
      justify-content: flex-end;
      color: white;
      background: url('$img_url'); background-size: cover;\">
        <span>" . $obj_name . "</span>
      </div>
      <div class=\"info\">
        <span class=\"normal\"><span class=\"dore\">Type d'objet: </span><span class=\"ital\">" . $obj_type . "</span></span>
        <span class=\"normal\"><span class=\"dore\">Localisation: </span><span class=\"ital\">" . $obj_loc . "</span></span>
        <span class=\"normal\"><span class=\"dore\">Createur: </span><span class=\"ital\">" . $obj_crea . "</span></span>
        <span class=\"normal\"><span class=\"dore\">Liens utiles:</span></span>
        <span class=\"normal\">" . $links . "</span>
      </div>
      ";
        return $text;
    }
}
