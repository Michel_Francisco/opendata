<?php
include("Database.php");
class User
{
    private $id_user = NULL;
    private $login = NULL;
    private $pwd = NULL;
    private $name = NULL;
    private $surname = NULL;
    private $email = NULL;
    private $type = NULL;

    function tryConnect($mail, $pass)
    {
        if (empty($mail) || empty($pass)) {
            return '<div class="alert alert-danger" role="alert">Veuillez completer tous les champs !</div>';
        }
        $db = new Database();
        $dbh = $db->connect();
        $hashed_pass = sha1($pass);
        $requete = "SELECT * FROM `users` WHERE email = '$mail' AND pwd_hash='$hashed_pass'";
        $resultat = $dbh->query($requete);
        $r = $resultat->fetch();
        if ($r == false) return '<div class="alert alert-danger" role="alert">Mot de passe ou email incorrect !</div>';
        if ($r["type"] == "contributor") {
            $perm = 1;
        }
        if ($r["type"] == "admin") {
            $perm = 2;
        }
        if ($r["type"] == "approver") {
            $perm = 3;
        }
        $id = $r['id_user'];
        $username = $r['login'];
        date_default_timezone_set('Europe/Paris');
        $date = date('y-m-d h:i:s');
        $requete2 = "INSERT INTO `sessions` (`id_session`, `date`, `user_id`) VALUES (NULL, '$date','$id')";
        $resultat = $dbh->query($requete2);
        $_SESSION['permission'] = $perm;
        $_SESSION['user'] = $id;
        $requete3 = "SELECT `id_session` FROM `sessions` WHERE `date` = '$date' AND `user_id`='$id'";
        $resultat2 = $dbh->query($requete3);
        $r2 = $resultat2->fetch();
        $_SESSION['session_id'] = $r2['id_session'];
        return $perm;
    }

    function mail($mail)
    {
        if (empty($mail)) {
            return '<div class="alert alert-danger" role="alert">Veuillez renseigner votre mail !</div>';
        }
        $db = new Database();
        $dbh = $db->connect();
        $resultat = $dbh->query("SELECT * FROM `users` WHERE email = '$mail'");
        $r = $resultat->fetch();
        if ($r == false) return '<div class="alert alert-danger" role="alert">Email inconnu !</div>';
        $subject = 'Récupération de mot de passe';
        $message = 'Bonjour votre mot de passe est: test';
        mail($mail, $subject, $message);
        return '<div class="alert alert-success" role="alert">Email envoyé avec succès !</div>';
    }

    function addUser($username, $pwd_hash, $name, $surname, $email, $type)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "INSERT INTO `users` (`id_user`, `login`, `pwd_hash`, `name`, `surname`, `email`, `type`) VALUES (NULL, '$username', '$pwd_hash', '$name', '$surname', '$email', '$type')";
        $dbh->query($req);
        return '<div class="alert alert-success" role="alert">Utilisateur ajouté avec succès !</div>';
    }

    function getusername($mail)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "SELECT `login` FROM `users` WHERE email = '$mail'";
        $res = $dbh->query($req);
        $r = $res->fetch();
        return $r;
    }
}
