<?php
include("Database.php");
class Map
{
    private $id_map = NULL;
    private $level = NULL;
    private $file_name = NULL;
    private $year_id = NULL;
    private $source = NULL;

    function addMap($level, $year_id, $source, $file_name, $oldname, $newname)
    {
        $db = new Database();
        $dbh = $db->connect();
        $req = "SELECT COUNT(`id_map`) FROM `maps` WHERE `level` = $level AND `year_id` = $year_id";
        $res = $dbh->query($req);
        $r = $res->fetch();
        if ($r[0] != '0') {
            return '<div class="alert alert-danger" role="alert">Cette Map existe déjà !</div>';
        }
        $req = "SELECT COUNT(`id_map`) FROM `maps` WHERE `file_name` = '$file_name'";
        $res = $dbh->query($req);
        $r = $res->fetch();
        if ($r[0] != '0') {
            return '<div class="alert alert-danger" role="alert">Ce nom de fichier est déjà utilisé !</div>';
        }
        $req = "INSERT INTO `maps` (`id_map`, `level`, `file_name`, `year_id`, `source`) VALUES (NULL, '$level', '$file_name', '$year_id', '$source')";
        $dbh->query($req);
        rename($oldname, $newname);
        return '<div class="alert alert-success" role="alert">Map ajoutée avec succès !</div>';
    }

    function deleteMap($id_map)
    {
        $db = new Database();
        $dbh = $db->connect();
        $requete = "SELECT `file_name` FROM `maps` WHERE id_map='$id_map'";
        $res = $dbh->query($requete);
        $r = $res->fetch();
        if ($r != '') {
            unlink('../images/plans/' . $r['file_name'] . '');
        }
        $requete = "DELETE FROM `maps` WHERE id_map='$id_map'";
        $dbh->query($requete);
        $requete = "DELETE FROM `markers` WHERE map_id='$id_map'";
        $dbh->query($requete);
        return '<div class="alert alert-success" role="alert">Map supprimée !</div>';
    }
}
