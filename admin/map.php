<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
    header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès à cette page
    header("location: ../contributor/markers.php");
} ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../styles/login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Gestion des map</title>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script type="text/javascript">
        $(document).ready(function() {
            $('#map_list').DataTable({
                "paging": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "pagingType": "numbers",

                "dom": '<"toptable"lf>t<"bottomtable" ip>',

                "language": {
                    "lengthMenu": "Montrer _MENU_ lignes par pages",
                    "zeroRecords": "Aucune ligne trouvée",
                    "info": "Page _PAGE_ sur _PAGES_",
                    "infoEmpty": "Pas de pages disponibles",
                    "infoFiltered": "(filtré à partir de _MAX_ enregistrements)",
                    "search": "Recherche"
                }
            });
        });
    </script>
</head>

<body>
    <?php include '../navbar.php';
    $change = false;
    require_once("../class/Map.php");
    $db = new Database();
    $dbh = $db->connect();
    ?>
    <div class="container">
        <div class="row">
            <div class="col-xl-4">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            <h5 class="card-title text-center">Ajouter une map</h5>
                            <div class="form-group">
                                <label for="etageForm">Etage</label>
                                <select class="form-control" id="etageForm" name="level">
                                    <option>0</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="dateForm">Date</label>
                                <select class="form-control" id="dateForm" name="date">
                                    <?php
                                    $requete = 'SELECT `year` FROM `years`';
                                    $resultat = $dbh->query($requete);
                                    while ($r = $resultat->fetch()) {
                                        echo '<option>' . $r['year'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group" for="sourceForm">
                                <label>Source</label>
                                <input class="form-control" name="source" placeholder="...">
                            </div>
                            <div class="form-group">
                                <label for="mapFile">Fichier d'image map</label>
                                <select class="form-control" id="fileForm" name="file">
                                    <?php
                                    if ($handle = opendir('./upload')) {
                                        while (false !== ($file = readdir($handle))) {
                                            if ($file != "." && $file != ".." && $file != ".gitkeep") {
                                                echo '<option>' . $file . '</option>';
                                            }
                                        }
                                        closedir($handle);
                                    }
                                    ?>
                                </select>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary" id="buttonFormStyle">Ajouter</button>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['level'])) {
                            if ($_POST['level'] != "") {
                                $level = $_POST['level'];
                            }
                        }
                        if (isset($_POST['date'])) {
                            if ($_POST['date'] != "") {
                                $change = true;
                                $db = new Database();
                                $dbh = $db->connect();
                                $date = $_POST['date'];
                                $req = "SELECT `id_year` FROM `years` WHERE `year` = '$date'";
                                $res = $dbh->query($req);
                                $date_id = $res->fetch();
                            }
                        }
                        if (isset($_POST['source'])) {
                            if ($_POST['source'] != "") {
                                $change = true;
                                $source = $_POST['source'];
                            }
                        }
                        if (isset($_POST['file'])) {
                            if ($_POST['file'] != "") {
                                $change = true;
                                $file = $_POST['file'];
                            }
                        }
                        if (isset($level) && isset($date) && isset($source) && $date_id['id_year'] != null) {
                            $oldname = 'upload/' . $file;
                            $newname = '../images/plans/' . $file;
                            $map = new Map;
                            $added = $map->addMap($level, $date_id['id_year'], $source, $file, $oldname, $newname);
                            echo $added;
                            echo '<script language="Javascript">
                            <!--
                            setTimeout(suite, 1000);
                            function suite() {
                                document.location.replace("map.php");
                            }
                            // -->
                            </script>';
                        } else if ($change) {
                            echo '<div class="alert alert-danger" role="alert">Veuillez remplir tous les champs !</div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xl-8">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-center"> Liste de vos map</h5>
                        <table id="map_list">
                            <thead style="background-color: #c9ae81;">
                                <tr style="text-align: center;">
                                    <th>Map Id</th>
                                    <th>Etage</th>
                                    <th>Année map</th>
                                    <th>Source</th>
                                    <th>Image de la map</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $requete = "SELECT * FROM `maps`";
                                $resultat = $dbh->query($requete);
                                while ($r = $resultat->fetch()) {
                                    $year_id = $r['year_id'];
                                    $r_year = $dbh->query("SELECT year FROM `years`,`maps` WHERE '$year_id'=id_year");
                                    $year = $r_year->fetch();
                                    echo '<tr>
                                                <td>' . $r['id_map'] . '</td>
                                                <td>' . $r['level'] . '</td>
                                                <td>' . $year['year'] . '</td>
                                                <td>' . $r['source'] . '</td>
                                                <td><a href="/opendata/images/plans/' . $r['file_name'] . '" target="_blank" style="color:black;">' . $r['file_name'] . '</td>
                                                <td>
                                                <button  style="border: none; background: none;" data-toggle="modal" data-target="#deleteButton' . $r['id_map'] . '">
                                                    <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="black" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                                    </svg>
                                                </button>

                                                <div class="modal fade" id="deleteButton' . $r['id_map'] . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <form id="formDelete" method="POST" action="">
                                                        <input type="hidden" name="id_delete_map" value="' . $r['id_map'] . '" >
                                                        <input type="hidden" name="id_delete_markers_year" value="' . $r['year_id'] . '" >
                                                        <input type="hidden" name="id_delete_markers_level" value="' . $r['level'] . '" >
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Suppression d\'une map (' . $r['id_map'] . ')</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                Cette action supprimera la map ainsi que tout les marqueurs liés à cet map.
                                                                </div>
                                                                <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                                                <button type="submit" class="btn btn-primary">Je suis sur !</button>
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>';
                                }
                                if (isset($_POST['id_delete_map'])) {
                                    $map = new Map;
                                    $deleted = $map->deleteMap($_POST['id_delete_map']);
                                    echo '<script language="Javascript">
                                        <!--
                                        setTimeout(suite, 1000);
                                        function suite() {
                                            document.location.replace("map.php");
                                        }
                                        // -->
                                    </script>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>