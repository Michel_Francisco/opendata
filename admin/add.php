<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
    header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès à cette page
    header("location: ../contributor/markers.php");
} ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../styles/login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Gestion des utilisateurs</title>
    <?php
    include '../navbar.php';
    require_once("../class/User.php");
    $db = new Database();
    $dbh = $db->connect();
    ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            <h5 class="card-title text-center">Ajouter un utilisateur</h5>
                            <div class="form-group" id="formsGroupStyle">
                                <label>Username</label>
                                <input class="form-control" name="add_username" placeholder="...">
                            </div>
                            <div class="form-group" id="formsGroupStyle">
                                <label>Password</label>
                                <input class="form-control" name="add_pwd" placeholder="...">
                            </div>
                            <div class="form-group" id="formsGroupStyle">
                                <label>Name</label>
                                <input class="form-control" name="add_name" placeholder="...">
                            </div>
                            <div class="form-group" id="formsGroupStyle">
                                <label>Surname</label>
                                <input class="form-control" name="add_surname" placeholder="...">
                            </div>
                            <div class="form-group" id="formsGroupStyle">
                                <label>Email address</label>
                                <input type="email" class="form-control" name="add_email" aria-describedby="emailHelp" placeholder="...">
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="add_type" id="inlineRadio1" value="admin">
                                <label class="form-check-label" for="inlineRadio1">Admin</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="add_type" id="inlineRadio2" value="contributor">
                                <label class="form-check-label" for="inlineRadio2">Contributor</label>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary" id="buttonFormStyle">Add</button>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['add_username'])) {
                            if ($_POST['add_username'] != "") {
                                $username = $_POST['add_username'];
                            }
                        }
                        if (isset($_POST['add_pwd'])) {
                            if ($_POST['add_pwd'] != "") {
                                $pwd_hash = sha1($_POST['add_pwd']);
                            }
                        }
                        if (isset($_POST['add_name'])) {
                            if ($_POST['add_name'] != "") {
                                $name = $_POST['add_name'];
                            }
                        }
                        if (isset($_POST['add_surname'])) {
                            if ($_POST['add_surname'] != "") {
                                $surname = $_POST['add_surname'];
                            }
                        }
                        if (isset($_POST['add_email'])) {
                            if ($_POST['add_email'] != "") {
                                $emailVerif = $_POST['add_email'];
                                $res = $dbh->query("SELECT COUNT(`id_user`) c FROM `users` WHERE `email`='$emailVerif'");
                                $res = $res->fetch();
                                if ($res['c'] > 0) echo '<div class="alert alert-danger" role="alert">Cette email existe déjà !</div>';
                                else $email = $_POST['add_email'];
                            }
                        }
                        if (isset($_POST['add_type'])) {
                            if ($_POST['add_type'] != "") {
                                $type = $_POST['add_type'];
                            }
                        }
                        if (isset($username) && isset($pwd_hash) && isset($name) && isset($surname) && isset($email) && isset($type)) {
                            $user = new User;
                            $added = $user->addUser($username, $pwd_hash, $name, $surname, $email, $type);
                            echo $added;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>