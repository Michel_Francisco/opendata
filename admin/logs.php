<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
  header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès à cette page
  header("location: ../contributor/markers.php");
} ?>

<!DOCTYPE html>
<html>

    <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../styles/login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Logs user</title>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script type="text/javascript">
        $(document).ready(function() {
        $('#user_log').DataTable({
            "paging": true,
            "ordering": true,
            "info": true,
            "searching": true,
            "pagingType": "numbers",

            "dom": '<"toptable"lf>t<"bottomtable" ip>',

            "language": {
            "lengthMenu": "Montrer _MENU_ lignes par pages",
            "zeroRecords": "Aucune ligne trouvée",
            "info": "Page _PAGE_ sur _PAGES_",
            "infoEmpty": "Pas de pages disponibles",
            "infoFiltered": "(filtré à partir de _MAX_ enregistrements)",
            "search": "Recherche"
            }
        });
        });
    </script>
    <?php
        require_once("../class/User.php");
        $db = new Database();
        $dbh = $db->connect();
    ?>
    </head>

    <?php
        if(!isset($_GET['user'])){
            echo '<script language="Javascript">
                document.location.replace("../gestion.php");
            </script>';
        }
    ?>
    <body>
        <div class="container">
            <div class="row">
            <div class="col-md-6 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <a href="../gestion.php" type="button" class="btn btn-secondary">Retour</a>
                        <table id="user_log">
                            <thead style="background-color: #c9ae81;">
                                <tr style="text-align: center;">
                                    <th>Id Utilisateur</th>
                                    <th>Heure de connexion</th>
                                    <th>Id Session</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $req = $dbh->query("SELECT * FROM `sessions` WHERE `user_id`=".$_GET['user']."");
                                    while ($r = $req->fetch()) {
                                        echo '  <tr style="text-align: center;">
                                                    <td> '.$r["user_id"].'</td>
                                                    <td> '.$r["date"].'</td>
                                                    <td> '.$r["id_session"].'</td>
                                                </tr>';

                                    }
                                ?>
                            </tbody>
                        </table>



                        </table>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>