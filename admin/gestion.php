<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
  header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès à cette page
  header("location: ../contributor/markers.php");
} ?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../styles/login.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <title>Gestion des utilisateurs</title>
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
  <script type="text/javascript">
    $(document).ready(function() {
      $('#users_contributor').DataTable({
        "paging": true,
        "ordering": true,
        "info": true,
        "searching": true,
        "pagingType": "numbers",

        "dom": '<"toptable"lf>t<"bottomtable" ip>',

        "language": {
          "lengthMenu": "Montrer _MENU_ lignes par pages",
          "zeroRecords": "Aucune ligne trouvée",
          "info": "Page _PAGE_ sur _PAGES_",
          "infoEmpty": "Pas de pages disponibles",
          "infoFiltered": "(filtré à partir de _MAX_ enregistrements)",
          "search": "Recherche"
        }
      });
    });
  </script>
  <?php include '../navbar.php';
  require_once("../class/User.php");
  $db = new Database();
  $dbh = $db->connect();
  ?>
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-body">
            <a class="btn btn-secondary " hreftype="button" href="add.php" style="margin-bottom: 0.5%;">
              Ajouter Utilisateur
            </a>
            <table id="users_contributor">
              <thead style="background-color: #c9ae81;">
                <tr style="text-align: center;">
                  <th>User id</th>
                  <th>Username</th>
                  <th>Name</th>
                  <th>Surname</th>
                  <th>Email</th>
                  <th>Type of user</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $requete = 'SELECT * FROM `users`';
                $resultat = $dbh->query($requete);
                while ($r = $resultat->fetch()) {
                  echo '<tr>
                          <td>' . $r['id_user'] . '</td>
                          <td>' . $r['login'] . '</td>
                          <td>' . $r['name'] . '</td>
                          <td>' . $r['surname'] . '</td>
                          <td>' . $r['email'] . '</td>
                          <td>' . $r['type'] . '</td>
                          <td>
                              <button  style="border: none; background: none;" data-toggle="modal" data-target="#deleteButton' . $r['id_user'] . '">
                                  <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="black" xmlns="http://www.w3.org/2000/svg">
                                      <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                  </svg>
                              </button>
                              
                              <div class="modal fade" id="deleteButton' . $r['id_user'] . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <form id="formDelete" method="POST" action="">
                                  <input type="hidden" name="id_delete_user" value="' . $r['id_user'] . '" >
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Suppression d\'un user (' . $r['id_user'] . ')</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                          Etes vous sur de vouloir supprimer cet utilisateur ?
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                          <button type="submit" class="btn btn-primary">Je suis sur !</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              </form>
                          </td>

                          <td>
                              <button style="border: none; background: none;" data-toggle="modal" data-target="#editButton' . $r['id_user'] . '">
                                  <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="black" xmlns="http://www.w3.org/2000/svg">
                                      <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                      <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                  </svg>
                              </button>

                              <div class="modal fade" id="editButton' . $r['id_user'] . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <form method="POST">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Edition de l\'utilisateur ' . $r['id_user'] . '</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                          <input type="hidden" name="user_id" value="' . $r['id_user'] . '" >
                                          <div class="form-group" id="formsGroupStyle">
                                            <label>Username</label>
                                            <input class="form-control" name="username" placeholder="' . $r['login'] . '">
                                          </div>
                                          <div class="form-group" id="formsGroupStyle">
                                            <label>Password</label>
                                            <input class="form-control" name="pwd" placeholder="...">
                                          </div>
                                          <div class="form-group" id="formsGroupStyle">
                                            <label>Name</label>
                                            <input class="form-control" name="name" placeholder="' . $r['name'] . '">
                                          </div>
                                          <div class="form-group" id="formsGroupStyle">
                                            <label>Surname</label>
                                            <input class="form-control" name="surname" placeholder="' . $r['surname'] . '">
                                          </div>
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" id="inlineRadio1" value="admin">
                                            <label class="form-check-label" for="inlineRadio1">Admin</label>
                                          </div>
                                          <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" id="inlineRadio2" value="contributor">
                                            <label class="form-check-label" for="inlineRadio2">Contributor</label>
                                          </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                          <button type="submit" class="btn btn-primary">Editer</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                          </td>
                          <td>
                            <a style="border: none; background: none;" href="./logs.php/?user='.$r['id_user'].'"">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="black" class="bi bi-info-circle-fill" viewBox="0 0 16 16">
                              <path fill-rule="evenodd" d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                            </svg>
                            </a>
                          </td>
                        </tr>';
                }
                if (isset($_POST['id_delete_user'])) {
                  $id_user = $_POST['id_delete_user'];
                  $requete = "DELETE FROM `users` WHERE id_user='$id_user'";
                  $dbh->query($requete);
                  echo '<script language="Javascript">
                        <!--
                        document.location.replace("gestion.php");
                        // -->
                        </script>';
                };

                if (isset($_POST['user_id'])) {
                  if ($_POST['user_id'] != "") {
                    $user_id = $_POST['user_id'];
                    $res = $dbh->query("SELECT COUNT(`id_user`) c FROM `users` WHERE `id_user`='$user_id'");
                    $res = $res->fetch();
                    if ($res['c'] == 0) echo "L'user ID choisi est incorrect !";
                    else {
                      if (isset($_POST['username'])) {
                        if ($_POST['username'] != "") {
                          $username = $_POST['username'];
                        }
                      }
                      if (isset($_POST['pwd'])) {
                        if ($_POST['pwd'] != "") {
                          $pwd_hash = sha1($_POST['pwd']);
                        }
                      }
                      if (isset($_POST['name'])) {
                        if ($_POST['name'] != "") {
                          $name = $_POST['name'];
                        }
                      }
                      if (isset($_POST['surname'])) {
                        if ($_POST['surname'] != "") {
                          $surname = $_POST['surname'];
                        }
                      }
                      if (isset($_POST['type'])) {
                        if ($_POST['type'] != "") {
                          $type = $_POST['type'];
                        }
                      }

                      $req = "UPDATE `users` SET ";
                      if (isset($username)) $req .=  "`login` = '$username', ";
                      if (isset($pwd_hash)) $req .=  "`pwd_hash` = '$pwd_hash', ";
                      if (isset($name)) $req .=  "`name` = '$name', ";
                      if (isset($surname)) $req .=  "`surname` = '$surname', ";
                      if (isset($type)) $req .=  "`type` = '$type' ";
                      else {
                        $res = $dbh->query("SELECT `type` FROM `users` WHERE `id_user`='$user_id'");
                        $res = $res->fetch();
                        $type = $res['type'];
                        $req .=  "`type` = '$type' ";
                      }
                      $req .=  "WHERE `id_user` ='$user_id'";
                      $result = $dbh->query($req);
                      echo '<script language="Javascript">
                        <!--
                        document.location.replace("gestion.php");
                        // -->
                        </script>';
                    }
                  } else echo "Veuillez choisi un User ID !";
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>