<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
    header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès à cette page
    header("location: ../contributor/markers.php");
} ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../styles/login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Ajout d'année</title>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script type="text/javascript">
        $(document).ready(function() {
            $('#years').DataTable({
                "paging": true,
                "ordering": true,
                "info": true,
                "searching": true,
                "pagingType": "numbers",

                "dom": '<"toptable"lf>t<"bottomtable" ip>',

                "language": {
                    "lengthMenu": "Montrer _MENU_ lignes par pages",
                    "zeroRecords": "Aucune ligne trouvée",
                    "info": "Page _PAGE_ sur _PAGES_",
                    "infoEmpty": "Pas de pages disponibles",
                    "infoFiltered": "(filtré à partir de _MAX_ enregistrements)",
                    "search": "Recherche"
                }
            });
        });
    </script>
    <?php
    include '../navbar.php';
    require_once("../class/Year.php");
    $db = new Database();
    $dbh = $db->connect();
    ?>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-12 col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <form method="POST">
                            <h5 class="card-title text-center">Ajouter une année</h5>
                            <div class="form-group" for="dateForm">
                                <label>Année</label>
                                <input class="form-control" id="dateForm" name="date" placeholder="YYYY" pattern="[0-9]{4}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                            </div>
                        </form>
                        <?php
                        if (isset($_POST['date'])) {
                            if ($_POST['date'] != "") {
                                $date = $_POST['date'];
                            }
                        }

                        if (isset($date, $dbh)) {
                            $year = new Year;
                            $bool = $year->verifYear($date, $dbh);
                            if ($bool == 1) {
                                $msg = $year->addYear($date, $dbh);
                                echo $msg;
                                echo '<script language="Javascript">
                                        <!--
                                        setTimeout(suite, 1000);
                                        function suite() {
                                            document.location.replace("year.php");
                                        }
                                        // -->
                                    </script>';
                            } else {
                                echo '<div class="alert alert-danger" role="alert"> Année déjà existante !</div>';
                            }
                        }

                        ?>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body">
                        <table id="years">
                            <thead style="background-color: #c9ae81;">
                                <tr style="text-align: center;">
                                    <th>Year Id</th>
                                    <th>Year</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $requete = 'SELECT * FROM `years`';
                                $resultat = $dbh->query($requete);
                                while ($r = $resultat->fetch()) {
                                    echo '<tr style="text-align: center;">
                                            <td>' . $r['id_year'] . '</td>
                                            <td>' . $r['year'] . '</td>
                                            <td>
                                                <button style="border: none; background: none;" data-toggle="modal" data-target="#deleteButton' . $r['id_year'] . '">
                                                    <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-trash-fill" fill="black" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                                                    </svg>
                                                </button>

                                                <div class="modal fade" id="deleteButton' . $r['id_year'] . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <form id="formDelete' . $r['id_year'] . '" method="POST" action="">
                                                        <input type="hidden" name="id_delete_year" value="' . $r['id_year'] . '" >
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Suppresion de la date ' . $r['year'] . '</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        Etes vous sur de vouloir supprimer la date avec cet id : ' . $r['id_year'] . '? 
                                                                        (Cette action supprimera aussi vos maps liés ainsi que vos marqueurs liés)
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                                                    <button type="submit" class="btn btn-primary">Je suis sur !</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </td>

                                            <td>
                                                <button style="border: none; background: none;" data-toggle="modal" data-target="#editButton' . $r['id_year'] . '">
                                                    <svg width="1.2em" height="1.2em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="black" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                                    </svg>
                                                </button>
          
                                        <div class="modal fade" id="editButton' . $r['id_year'] . '" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <form method="POST">
                                            <div class="modal-dialog" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edition de l\'année ' . $r['id_year'] . '</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                    <div class="modal-body">
                                                        <input type="hidden" name="year_id_edit" value="' . $r['id_year'] . '" >
                                                        <div class="form-group" id="formsGroupStyle">
                                                            <label>Année</label>
                                                            <input class="form-control" name="year_edited" placeholder="' . $r['year'] . '">
                                                        </div>
                                                    </div>
                                                  <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Editer</button>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </form>
                                        </div>

                                            </td>
                                        </tr>';
                                }

                                if(isset($_POST['year_id_edit'])){
                                    if (isset($_POST['year_id_edit'])) {
                                        if ($_POST['year_id_edit'] != "") {
                                          $yearEdit = $_POST['year_id_edit'];
                                        }
                                    }
                                }

                                if(isset($_POST['year_edited'])){
                                    if (isset($_POST['year_edited'])) {
                                        if ($_POST['year_edited'] != "") {
                                          $newYear = $_POST['year_edited'];
                                        }
                                    }
                                }
                                
                                
                                if(isset($newYear)&&isset($yearEdit)){
                                    $year = new Year;
                                    $edited = $year->updateYear($dbh,$yearEdit,$newYear);
                                    echo $edited;
                                    echo '<script language="Javascript">
                                        <!--
                                        setTimeout(suite, 1000);
                                        function suite() {
                                            document.location.replace("year.php");
                                        }
                                        // -->
                                    </script>';
                                }

                                if (isset($_POST['id_delete_year'])) {
                                    $year = new Year;
                                    $deleted = $year->deleteYear($_POST['id_delete_year']);
                                    echo '<script language="Javascript">
                                        <!--
                                        setTimeout(suite, 1000);
                                        function suite() {
                                            document.location.replace("year.php");
                                        }
                                        // -->
                                    </script>';
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>