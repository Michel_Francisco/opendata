<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
    header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès a la navbar entière 
    include '../navbarContributor.php';
} else include '../navbar.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../styles/login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>Gestion des objets historiques</title>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
</head>

<body>
    <?php
    require_once("../class/Objects.php");
    require_once("../class/Database.php");
    ?>
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-xl-3">
                    <!-- Center the card -->
                </div>
                <div class="col-md-5 col-xl-6">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST">
                                <div id="formDiv">
                                    <h5 class="card-title text-center">Créer un objet historique</h5>
                                    <div class="form-group">
                                        <label for="typeForm">Type</label>
                                        <select class="form-control" id="etageForm" name="type">
                                            <option>Personne</option>
                                            <option>Piece</option>
                                            <option>Objet</option>
                                        </select>
                                    </div>
                                    <div class="form-group" for="labelForm">
                                        <label>Label</label>
                                        <input class="form-control" name="label" placeholder="...">
                                    </div>
                                    <div class="form-group" for="wikidata">
                                        <label>Lien Wikidata</label>
                                        <input class="form-control" name="wikidata" placeholder="...">
                                    </div>

                                    <div class="form-group" for="source1">
                                        <label>Lien Source</label>
                                        <input class="form-control" name="source1" placeholder="...">
                                    </div>
                                </div>
                        </div>
                        <div class="form-group">
                            <button type="button" id="addSourceLink" class="btn btn-primary" style="margin-left:30%">Ajouter un lien source</button>
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary" id="buttonFormStyle">Ajouter</button>
                        </div>
                        </form>
                        <?php
                        if (isset($_POST['type'])) {
                            if ($_POST['type'] != "") {
                                $type = $_POST['type'];
                            }
                        }
                        if (isset($_POST['wikidata'])) {
                            if ($_POST['wikidata'] != "") {
                                $wikidata = $_POST['wikidata'];
                            }
                        }
                        if (isset($_POST['label'])) {
                            if ($_POST['label'] != "") {
                                $label = $_POST['label'];
                            }
                        }

                        for ($i = 0; $i <= 10; $i++) {
                            if (isset($_POST['source' . $i])) {
                                if ($_POST['source' . $i] != "") {
                                    ${'source' . $i} = $_POST['source' . $i];
                                }
                            }
                        }

                        if (isset($type) && isset($wikidata) && isset($label)) {
                            $obj = new Objects;
                            $added = $obj->addObj($type, $label, $wikidata);
                            echo $added;
                            for ($i = 1; $i <= 10; $i++) {
                                if (isset(${'source' . $i})) {
                                    $link = ${'source' . $i};
                                    $add = $obj->addLink($label, $link);
                                }
                            }
                            echo '<script language="Javascript">
                                <!--
                                    setTimeout(suite, 1000);
                                    function suite() {
                                    document.location.replace("h_objects.php");
                                    }
                                // -->
                                </script>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3">
            <!-- Center the card -->
        </div>
        </div>
        </div>
    </body>

</html>

<script>
    $(document).ready(function() {
        var i = 1;
        $('#addSourceLink').click(function() {
            i++
            if (i <= 10) {
                $('#formDiv').append(` 
                                        <div class="form-group" for="source` + i + `">
                                            <label>Lien Source ` + i + `</label>
                                            <input class="form-control" name="source` + i + `" placeholder="...">
                                        </div>`);
            }
            if (i == 11) {
                $('#formDiv').append('<div class="alert alert-danger" role="alert">Vous ne pouvez pas mettre plus de 10 liens sources !</div>');
            }

        });
    });
</script>