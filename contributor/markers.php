<?php session_start();
if (!isset($_SESSION["permission"])) { //Vérifie si une session user est en cours sinon renvoi à la connexion
    header("location: ../auth/login.php");
}
if ($_SESSION["permission"] < 2) { // Le contributeur n'a pas accès a la navbar entière 
    include '../navbarContributor.php';
} 
else include '../navbar.php';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../styles/login.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <title>Gestion des marqueurs</title>

    <?php
    require_once("../class/Marker.php");
    $db = new Database();
    $dbh = $db->connect();
    ?>
    <style>
        #map {
            width: 80vw;
            height: 70vh;
        }
    </style>
</head>

<body>
    <div class="column">
        <div class="row">
            <div class="col-xl-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Sélection de la carte</h5>
                        <form method="POST">
                            <div class="form-group">
                                <label for="mapFile">Fichier d'image map</label>
                                <select class="form-control" id="fileForm" name="file">
                                    <?php
                                    if ($handle = opendir('../images/plans/')) {
                                        while (false !== ($file = readdir($handle))) {
                                            if ($file != "." && $file != ".." && $file != ".gitkeep") {
                                                echo '<option>' . $file . '</option>';
                                            }
                                        }
                                        closedir($handle);
                                    }
                                    ?>
                                </select>
                                <div>
                                    <button type="submit" class="btn btn-primary mt-2">Sélectionner</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-xl-8" id="map">
                <!-- MAP LEAFLET -->
            </div>
                <div id="popupDelete"></div>                
            <?php
            if (isset($_POST['id_delete_marker'])){
                $id_marker = $_POST['id_delete_marker'];
                $requete = "DELETE FROM `markers` WHERE `id_marker`='$id_marker'";
                $dbh->query($requete);
            }
            if (isset($_POST['file'])) {
                $map = $_POST['file'];
                $dim = getimagesize('../images/plans/' . $map);
            }
            ?>

            <script>
                var map = L.map('map', {
                    crs: L.CRS.Simple,
                    minZoom: -5,
                });
                //Taille de la map
                <?php echo "var x = $dim[0];"; ?>
                <?php echo "var y = $dim[1];"; ?>
                var bounds = [
                    [0, 0],
                    [y, x]
                ];
                <?php if (isset($map)) echo "var image = L.imageOverlay('../images/plans/$map', bounds).addTo(map);"; ?>

                //Gestion de l'icone
                var icone = L.icon({
                    iconUrl: '../images/marker.png',
                    iconAnchor: [19, 50], // point of the icon which will correspond to marker's location
                    popupAnchor: [0, -45] // point from which the popup should open relative to the iconAnchor
                });

                var coord = L.latLng([-100000000, 10000000]);
                var marker = L.marker(coord, {
                    icon: icone
                }).addTo(map);

                //Fonction onClick qui update la position du marker ci-dessus et qui nous renvoi ses coordonnées
                map.on('click', function(e) {
                    var coord = e.latlng.toString().split(',')
                    var lat = coord[0].split('(')
                    var long = coord[1].split(')')
                    console.log("" + lat[1] + "," + long[0]);
                    marker.setLatLng(e.latlng);
                    document.getElementById("long").value = long[0]
                    document.getElementById("lat").value = lat[1]
                });

                <?php
                    $marker = new Marker;
                    $marker->printMarkersAdmin($dbh,$map);
                ?>

                //Affiche le centre de la map 
                map.setView([y / 2, x / 2], 1);
            </script>

            <div class="col-lg-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Création de marqueur</h5>
                        <form method="POST">
                            <div class="form-group">
                                <label for="MapName">Map</label>
                                <?php if (isset($map)) echo '<input class="form-control" name="name_map" id="mapn" value ="' . $map . '" readonly>';
                                else echo '<input class="form-control" name="name_map" id="mapn" readonly>' ?>
                            </div>
                            <div class="form-group">
                                <label for="longitudeInput">Longitude</label>
                                <input class="form-control" name="longi" id="long" readonly>
                            </div>
                            <div class="form-group">
                                <label for="latitudeInput">Latitude</label>
                                <input class="form-control" name="latit" id="lat" readonly>
                            </div>
                            <div class="form-group">
                                <label for="objForm">Objet historique</label>
                                <select class="form-control" id="objForm" name="objet">
                                    <?php
                                    $requete = 'SELECT `name_obj` FROM `objects`';
                                    $resultat = $dbh->query($requete);
                                    while ($r = $resultat->fetch()) {
                                        echo '<option>' . $r['name_obj'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Envoyer</button>
                        </form>
                        <?php

                        if (isset($_POST['name_map'])) {
                            if ($_POST['name_map'] != "") {
                                $map = $_POST['name_map'];
                            }
                        }

                        if (isset($_POST['longi'])) {
                            if ($_POST['longi'] != "") {
                                $longi = $_POST['longi'];
                            }
                        }

                        if (isset($_POST['latit'])) {
                            if ($_POST['latit'] != "") {
                                $latit = $_POST['latit'];
                            }
                        }

                        if (isset($_POST['objet'])) {
                            if ($_POST['objet'] != "") {
                                $name_obj = $_POST['objet'];
                            }
                        }

                        if (isset($name_obj) && isset($longi) && isset($latit) && isset($map)) {
                            $req = $dbh->query("SELECT `id_map` FROM `maps` WHERE `file_name` = '$map'");
                            $r = $req->fetch();
                            $idmap = $r['id_map'];
                            $marker = new Marker;
                            $added = $marker->addMarker($longi, $latit, $name_obj, $idmap);
                            echo $added;

                            echo '<script language="Javascript">
                                <!--
                                setTimeout(suite, 1000);
                                function suite() {
                                    document.location.replace("markers.php");
                                }
                                // -->
                                </script>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>