<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
  <a class="navbar-brand">Immersailles</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topheader" aria-controls="topheader" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="topheader">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="..">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../contributor/markers.php">Marqueurs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../contributor/h_objects.php">Objets historiques</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../auth/logout.php">Déconnexion</a>
      </li>
    </ul>
  </div>
</nav>
